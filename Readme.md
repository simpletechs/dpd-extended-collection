## dpd-extended-collection - a better collection for deployd

This module adds a new type of Collection to deployd, i.e. `extended-collection`s.
While providing the same basic API to the outside world, extended Collections were built with two things in mind: Security and Performance.
This makes them better suited for larger deployd instanced than the default collections.

### Requirements

* deployd (you'd have guessed that, probably :-))
* dpd-extended-collection

### Installation

In your app's root directory, type `npm install dpd-extended-collection` into the command line or [download the source](https://bitbucket.org/simpletechs/dpd-extended-collection). This should create a `dpd-extended-collection` directory in your app's `node_modules` directory.

See [Installing Modules](http://docs.deployd.com/docs/using-modules/installing-modules.md) for details.

### Setup

No actual setup is required, you simple choose "ExtendedCollection" instead of "Collection" when creating a new resource and this earns you all the benefits.

### Usage

Note that the Event API changes severly in comparison to deployd's default collection!
See DOCS.md for details.

### Testing

This module is covered by tests that are run against the latest supported version of `deployd`.

The latest build on `master`:

[![wercker status](https://app.wercker.com/status/6cb916749fd5d9a88a400c1f49d90147/m/master "wercker status")](https://app.wercker.com/project/bykey/6cb916749fd5d9a88a400c1f49d90147)

The latest build on `develop`:

[![wercker status](https://app.wercker.com/status/6cb916749fd5d9a88a400c1f49d90147/m/develop "wercker status")](https://app.wercker.com/project/bykey/6cb916749fd5d9a88a400c1f49d90147)


### Credits

`dpd-extended-collection` is the work of [simpleTechs.net](https://www.simpletechs.net)