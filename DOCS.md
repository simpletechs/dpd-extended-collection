# Extended Collection API Documentation

## Batch Requests

Extended Collection was started with the intention to allow batch requests, meaning it allows to send/request arrays of data and update multiple data rows with only one http request, this can be done by just passing an array of objects to the corresponding functions. For DELETE you can also use dpd.js with ```dpd.extendedCollection.del(['123', '456'], callback)```.

### Errors In PUT Batch Requests

While non-batch requests will still be return errors with the status code 400, batch requests however could have errors for subrequests which will lead to the status code of 207. You can detect errors by checking for 207 and then checking if array items of the answers contains the ```status: 400``` property.


## Events

The mayor difference between Extended Collection events and events of the default collection is that each request (GET, PUT = UPDATE, POST = INSERT, DELETE) consists of two events, one before any interaction with the database happens and one (*ON BEFORE*) and one after there was interaction with the db. 

Each event gets an array of data by using the ```data``` variable (*this* works too, but is not recommended), even for non batch requests. 
If a BEFORE event gets canceled or throws an error (via *error('key', 'message')*) the AFTER event will not be triggered. BEFORE also allows to change the query that will happen between the two events.

Next to **data/this** there is also the **response** variable in the *AFTER* events which can be overriden to change the response of the collection request. Except for DELETE it will be the same object as **data**, but assigning a new object to data will have no effect on the response since it breaks the internal reference.

### Event Storage / Inter-Event Communication

To pass data from a BEFORE event to it's corresponding AFTER event, you can use the **eventStorage** interfaces which are ```set```, ```get``` and ```contains``` as methods. This is just for a convenient way of communicating for the developer, without having to change anything that might influence the state of the system.

```eventStorage.set(property, value)``` sets an arbitrary value with the ```property``` (string). This is useful inside the BEFORE events.

```eventStorage.get(property)``` returns a previously set value. Most-likely only used in AFTER events.

```eventStorage.contains(property)``` returns true if a value was set for that property.

### Event-Types
#### GET

##### ON BEFOREGET
This event should be used to change the query. This can be achieved with ```setQuery({mongoQuery})```.

**data**: the query (not necessarily an array)

##### ON AFTERGET

**data**: array of requested data

**response**: the response object for the user request



#### POST

##### ON BEFOREINSERT

**data**: the request (as an array);

##### ON AFTERINSERT

**data**: array of database responses (the inserted objects)

**response**: the response object for the user request



#### PUT

##### ON BEFOREUPDATE

**data**: the request (as an array)

##### ON AFTERUPDATE

**data**: array of changed objects

**response**: the response object for the user request

```json
{
    "count": number of objects updated,
    "id": id of the changed object
}
```

#### DELETE

##### ON BEFOREDELETE

**data**: the requested item ids

##### ON AFTERDELETE

**data**: array of deleted objects, can contain ```undefined``` if the item was not found
*response*: the response that will be send to the client arrray or single object with ```count``` and ```id``` property

```json
{
    "count": number of objects updated,
    "id": id of the changed object
}
```

