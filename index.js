var validation = require('deployd/node_modules/validation'),
    util = require('util'),
    fs = require('fs'),
    path = require('path'),
    Resource = require('deployd/lib/resource'),
    db = require('deployd/lib/db'),
    EventEmitter = require('events').EventEmitter,
    debug = require('debug')('extended-collection'),
    path = require('path'),
    Script = require('deployd/lib/script'),
    _ = require('deployd/node_modules/underscore'),
    when = require('when'),
    whenSequence = require('when/sequence'),
    pkg = require('./package.json');

console.log('Enabling ExtendedCollection version ', pkg.version);

/**
 * A `Collection` validates incoming requests then proxies them into a `Store`.
 *
 * Options:
 *
 *   - `path`                the base path a resource should handle
 *   - `config.properties`   the properties of objects the collection should store
 *   - `db`                  the database a collection will use for persistence
 *
 * @param {Object} options
 */

function ExtendedCollection(name, options) {
  Resource.apply(this, arguments);

  var config = this.config;
  if(config) {
    this.properties = config.properties;
  }
  if (options) {
    this.store = options.db && options.db.createStore(this.name);
  }
}
util.inherits(ExtendedCollection, Resource);
ExtendedCollection.external = {};
ExtendedCollection.prototype.clientGeneration = true;

ExtendedCollection.events  = ['BeforeGet', 'AfterGet', 'BeforeInsert', 'AfterInsert', 'BeforeUpdate', 'AfterUpdate', 'BeforeDelete', 'AfterDelete'];
ExtendedCollection.dashboard = {
    path: path.join(__dirname, 'dashboard')
  , pages: ['Config', 'Properties', 'Data', 'Events', 'API']
  , scripts: [
      '/js/lib/jquery-ui-1.8.22.custom.min.js'
    , '/js/lib/knockout-2.1.0.js'
    , '/js/lib/knockout.mapping.js'
    , '/js/util/knockout-util.js'
    , '/js/util/key-constants.js'
    , '/js/util.js'
  ]
};

ExtendedCollection.basicDashboard = {
  settings: [{
    name        : 'allowGet',
    type        : 'checkbox',
    description : 'Allow external access to GET. internal requests (from other events) will work either way!'
  }, {
    name        : 'allowInsert',
    type        : 'checkbox',
    description : 'Allow external access to POST.'
  }, {
    name        : 'allowUpdate',
    type        : 'checkbox',
    description : 'Allow external access to PUT.'
  }, {
    name        : 'allowDelete',
    type        : 'checkbox',
    description : 'Allow external access to DELETE.'
  }]
};

var Collection = ExtendedCollection;

/**
 * Validate the request `body` against the `Collection` `properties`
 * and return an object containing any `errors`.
 *
 * @param {Object} body
 * @param {Bool} create
 * @param {Object} currentContext
 * @return {Object} errors
 */

ExtendedCollection.prototype.validate = function (body, create, ctx) {
  if(!this.properties) this.properties = {};

  var keys = Object.keys(this.properties)
    , props = this.properties
    , errors = {};

  keys.forEach(function (key) {
    var prop = props[key]
      , val = body[key]
      , type = prop.type || 'string';

    debug('validating %s against %j', key, prop);

    if(validation.exists(val)) {
      // coercion
      if(type === 'number') val = Number(val);
      if(type === 'id') type = 'string';

      if(!validation.isType(val, type)) {
        debug('failed to validate %s as %s', key, type);
        errors[key] = 'must be a ' + type;
      }
    } else if(prop.required) {
      debug('%s is required', key);
      if(create) {
        errors[key] = 'is required';
      }
    } else if(type === 'boolean') {
      body[key] = false;
    }
  });

  if(Object.keys(errors).length) return errors;
};

/**
 * Sanitize the request `body` against the `Collection` `properties`
 * and return an object containing only properties that exist in the
 * `Collection.config.properties` object.
 *
 * @param {Object} body
 * @return {Object} sanitized
 */

ExtendedCollection.prototype.sanitize = function (body) {
  if(!this.properties) return {};

  var sanitized = {}
    , props = this.properties
    , keys = Object.keys(props);

  keys.forEach(function (key) {
    var prop = props[key]
    , expected = prop.type
    , val = body[key]
    , actual = typeof val;
    // skip properties that do not exist
    if(!prop) return;

    if(expected == actual) {
      sanitized[key] = val;
    } else if (expected === 'array' && Array.isArray(val)) {
      sanitized[key] = val;
    } else if (expected === 'id' && actual === 'string') {
      sanitized[key] = val;
    } else if(expected === 'number' && actual === 'string') {
      sanitized[key] = parseFloat(val);
    } else if(expected === 'string' && actual === 'number') {
      sanitized[key] = '' + val;
    }
  });

  return sanitized;
};

ExtendedCollection.prototype.sanitizeQuery = function (query) {
  var sanitized = {}
    , props = this.properties || {}
    , keys = query && Object.keys(query);

  keys && keys.forEach(function (key) {
    var prop = props[key] || props[key.split('.')[0]]
    , expected = prop && prop.type
    , val = query[key]
    , actual = typeof val;

    // skip properties that do not exist, but allow $ queries and id
    if(!prop && key.indexOf('$') !== 0 && key !== 'id') return;

    // hack - $limitRecursion and $skipEvents are not mongo properties so we'll get rid of them, too
    if (key === '$limitRecursion') return;
    if (key === '$skipEvents') return;

    if(expected == 'string' && actual == 'number') {
      sanitized[key] = '' + val;
    } else if(expected == 'number' && actual == 'string') {
      sanitized[key] = parseFloat(val);
    } else if(expected == 'boolean' && actual != 'boolean') {
      sanitized[key] = (val === 'true') ? true : false;
    } else if(expected == 'object') {
      sanitized[key] = val;
    }  else if (typeof val !== 'undefined') {
      sanitized[key] = val;
    }
  });

  return sanitized;
};
/**
 * Handle an incoming http `req` and `res` and execute
 * the correct `Store` proxy function based on `req.method`.
 *
 *
 * @param {ServerRequest} req
 * @param {ServerResponse} res
 */
ExtendedCollection.prototype.isExternallyAllowed = function(method) {
  switch(method && method.toUpperCase()) {
    case 'GET':
      return this.config.allowGet;
    case 'POST':
      return this.config.allowInsert;
    case 'PUT':
      return this.config.allowUpdate;
    case 'DELETE':
      return this.config.allowDelete;
    default: 
      return false;
  }
};

ExtendedCollection.prototype.handle = function (ctx) {
  //console.log('properties', this.properties);
  if(!((ctx.req && ctx.req.internal) || (ctx.req && ctx.req.session && ctx.req.session.isRoot))) {
    // this request is neither internal nor is the user root
    // check if the request should be handled at all
    if(!this.isExternallyAllowed(ctx.req.method)) {
      return ctx.done('resource not found');
    }
  }

  // set id one wasnt provided in the query
  ctx.query.id = ctx.query.id || this.parseId(ctx) || (ctx.body && ctx.body.id);


  if (ctx.req.method == "GET" && ctx.query.id === 'count') {
    delete ctx.query.id;
    this.count(ctx, ctx.done);
    return;
  }

  if (ctx.req.method == "GET" && ctx.query.id === 'index-of') {
    delete ctx.query.id;
    var id = ctx.url.split('/').filter(function(p) { return p; })[1];
    this.indexOf(id, ctx, ctx.done);
    return;
  }
  switch(ctx.req.method) {
    case 'GET':
      this.find(ctx, ctx.done);
    break;
    case 'PUT':
      // if (typeof ctx.query.id != 'string' && !ctx.req.isRoot 
      //   && Object.prototype.toString.call(ctx.body) !== '[object Array]') {
      //   ctx.done("single queries must provide id to update an object");
      //   break;
      // }
      this.update(ctx, ctx.done);
      break;
    case 'POST':
      this.insert(ctx, ctx.done);
    break;
    case 'DELETE':
      this.remove(ctx, ctx.done);
    break;
  }
};


/**
 * Parse the `ctx.url` for an id
 *
 * @param {Context} ctx
 * @return {String} id
 */

ExtendedCollection.prototype.parseId = function(ctx) {
  if(ctx.url && ctx.url !== '/') return ctx.url.split('/')[1];
};

ExtendedCollection.prototype.count = function(ctx, fn) {
  if (ctx.session.isRoot) {
    var collection = this
      , store = this.store
      , sanitizedQuery = this.sanitizeQuery(ctx.query || {});

    store.count(sanitizedQuery, function (err, result) {
      if (err) return fn(err);

      fn(null, {count: result});
    });
  } else {
    fn({
      message: "Must be root to count",
      statusCode: 403
    });
  }
};

ExtendedCollection.prototype.indexOf = function(id, ctx, fn) {
  if (ctx.session.isRoot) {
    var collection = this
      , store = this.store
      , sanitizedQuery = this.sanitizeQuery(ctx.query || {});

    sanitizedQuery.$fields = {id: 1};

    store.find(sanitizedQuery, function (err, result) {
      if (err) return fn(err);

      var indexOf = result.map(function(r) { return r.id }).indexOf(id);

      fn(null, {index: indexOf});
    });
  } else {
    fn({
      message: "Must be root to get index",
      statusCode: 403
    });
  }
};

/**
 * Find all the objects in a collection that match the given
 * query. Then execute its get script using each object.
 *
 * @param {Context} ctx
 * @param {Function} fn(err, result)
 */

ExtendedCollection.prototype.find = function (ctx, fn) {
  var collection = this,
      store = this.store,
      query = ctx.query || {},
      session = ctx.session,
      client = ctx.dpd,
      errors = {},
      data,
      sanitizedQuery,
      domain;

  function done(err, result) {
    debug("Get listener called back with", err || result);
    if(typeof query.id === 'string' && (result && result.length === 0) || !result) {
      err = err || {
        message: 'not found',
        statusCode: 404
      };
      debug('could not find object by id %s', query.id);
    }
    if(err) {
      return fn(err);
    }
    if(typeof query.id === 'string' && Array.isArray(result)) {
      return fn(null, result[0]);
    }

    fn(null, result);
  }
  debug('query before BeforeGet %j; sanitized %j', query, sanitizedQuery);
  var eventStorage = {};
  domain = createDomain(query, errors, eventStorage);
  domain.setQuery = function(q){
    query = q;
  };
  this.runEvent(collection.events.BeforeGet, ctx, domain, function(err, res) {
    if(err) return done(err);
    if(domain.hasErrors()) return fn({errors: errors});
    sanitizedQuery = collection.sanitizeQuery(query);
    debug('query after BeforeGet %j; sanitized %j', query, sanitizedQuery);

    store.find(sanitizedQuery, function (err, result) {
      debug("Find Callback");
      if(err) return done(err);
      debug('found %j', result || 'none');

      if(!result) result = [];
      if(!Array.isArray(result)) {
        result = result? [result] : [];
      }

      domain = createDomain(result, errors, eventStorage);
      collection.runEvent(collection.events.AfterGet, ctx, domain, function(err) {
        debug('AfterGet callback with result %j and error %j', domain._result, err);
        if(err) return done(err);
        if(domain.hasErrors()) return fn({errors: errors});
        done(null, domain._result);
      });
    });
  });
};

/**
 * Execute a `delete` event script, if one exists, using each object found.
 * Then remove a single object that matches the `ctx.query.id`. Finally call
 * `fn(err)` passing an `error` if one occurred.
 *
 * @param {Context} ctx
 * @param {Function} fn(err)
 */

ExtendedCollection.prototype.remove = function (ctx, fn) {
  var collection = this,
      store = this.store,
      session = ctx.session,
      query = ctx.query,
      body = ctx.body,
      isBatchQuery = false,
      errors = {},
      requestedItems,
      promises = [],
      domain;

  if(!Array.isArray(body) && ctx.query.id === undefined){ 
    // dpd.del([id1, id2]) are as query parameters ?0=id1&1=id2
    delete ctx.query.id;
    var keys = Object.keys(ctx.query),
        indexes = [], 
        invalidState = false;

    keys.forEach(function(key){
      var i = parseInt(key);
      if(!isNaN(i)) {
        indexes.push(i);
      } else {
        invalidState = true;
        return fn('invalid query parameter '+key+' received. Expected array notation.');
      }
    });
    if(invalidState) return;
    indexes.sort();
    var id;
    body = [];
    indexes.forEach(function(index){
      id = ctx.query[index];
      body.push({id: id});
    });
  }

  if(Array.isArray(body)) {
    requestedItems = body;
    isBatchQuery = true;
  } 
  if(!Array.isArray(body) && body) {
    // the user supplied a single item only

    body.id = body.id || ctx.query.id;
    if(typeof body.id === 'number'){ // random ids can be without characters
      body.id = ''+body.id;
    }
    ctx.query.id = body.id;
    if (typeof body.id !== 'string')
      return ctx.done('You must include a query with an id when deleting an object from a collection.');
      
    requestedItems = [body];
  }
  
  var eventStorage = {};
  domain = createDomain(requestedItems, errors, eventStorage);
  collection.runEvent(collection.events.BeforeDelete, ctx, domain, function(err, res) {
    if(err) return fn(err);
    if(domain.hasErrors()) return fn({errors: errors});

    requestedItems.forEach(function(requestedItem){
      var deferred = when.defer(),
          id = requestedItem.id;
      promises.push(deferred.promise);
      store.find({id: requestedItem.id}, function (err, oldItem) {
        if(err) return deferred.reject(err);

        // do not break execution since this would lead to an undefined state
        if(!requestedItem.id || !oldItem || Array.isArray(oldItem) || Object.keys(oldItem).length === 0){
          return deferred.resolve({removeResult: {count: 0, id: id}});
        }

        store.remove({id: requestedItem.id}, function(err, result) {
          if(err) return deferred.reject(err);

          result.id = id;
          deferred.resolve({oldItem: oldItem, removeResult: result || {count: 0, id: id}});
        });
      });
    });

    when.all(promises).then(function(results){
      if(session.emitToAll) session.emitToAll(collection.name + ':changed');
      var oldItems = [],
          removeResults = [];
      results.forEach(function(result){
        if(result.errors) {
          removeResults.push(result);
        } else {
          oldItems.push(result.oldItem);
          removeResults.push(result.removeResult);
        }
      });

      domain = createDomain(oldItems, errors, eventStorage);
      domain._result = removeResults;

      if(!isBatchQuery) domain._result = removeResults[0];
      domain['this'] = domain.data = oldItems;
      
      collection.runEvent(collection.events.AfterDelete, ctx, domain, function(err) {
        if(err) return fn(err);
        if(domain.hasErrors()) return fn({errors: errors});

        fn(null, domain._result);
      });
    }, function(error){
      return fn(error);
    });
  });
};

/**
 * Execute the onPost or onPut listener. If it succeeds,
 * save the given item in the collection.
 *
 * @param {Context} ctx
 * @param {Function} fn(err, result)
 */
ExtendedCollection.prototype.insert = function (ctx, fn) {
  var collection = this,
      store = this.store,
      session = ctx.session,
      items = [],
      body = ctx.body,
      isBatchQuery = false, 
      query = ctx.query || {},
      client = ctx.dpd,
      errors = {},
      domain;

  if(Array.isArray(body)) {
    items = body;
    isBatchQuery = true;
  } else if(!Array.isArray(body) && body) {
    // the user supplied a single item only
    items = [body];
  }
  if(!items.length) return fn('You must include an object when saving or updating.');

  // run the beforeInsert event with RAW data from the request
  var eventStorage = {};
  domain = createDomain(items, errors, eventStorage);
  collection.runEvent(collection.events.BeforeInsert, ctx, domain, function(err) {
    if(err) return fn(err);
    if(domain.hasErrors()) return fn({errors: errors});

    var item;
    for(var i = 0; i < items.length; i++) {
      item = items[i];
      if(item.id){
        fn('POST does not accept objects with an id property');
        return;
      }
    }    


    for(var i = 0; i < items.length; i++) {
      item = items[i];

      // build command object
      var commands = {};
      Object.keys(item).forEach(function (key) {
        if(item[key] && typeof item[key] === 'object' && !Array.isArray(item[key])) {
          Object.keys(item[key]).forEach(function (k) {
            if(k[0] == '$') {
              commands[key] = item[key];
            }
          });
        }
      });

      items[i] = collection.sanitize(item);
      item = items[i];

      collection.execCommands('update', item, commands);
      var errs = collection.validate(item, true, ctx);

      if(errs) {
        return fn({errors: errs});
      }

      // generate id AFTER event listener. Before Insert is not supposed to use the id
      item.id = store.createUniqueIdentifier();
    }

    store.insert(items, function(err, results) {
      if(err) return fn(err);

      if(session && session.emitToAll) session.emitToAll(collection.name + ':changed');
      
      // double check
      if(!Array.isArray(results)){
        results = results? [results]: [];
      }
      domain = createDomain(results, errors, eventStorage);
      collection.runEvent(collection.events.AfterInsert, ctx, domain, function(err) {
        if(err) return fn(err);
        if(domain.hasErrors()) return fn({errors: errors});

        // return single objects if the client sent a single object
        if(!isBatchQuery && !domain.hasChangedResult() && results.length <= 1){
            return fn(null, results.length? results[0] : {});
        }
        return fn(null, domain._result);
        
      });
    });
  });
};


/**
 * Execute the onBeforeUpdate listener. If it succeeds,
 * save the given item in the collection and execute
 * onAfterUpdate.
 *
 * @param {Context} ctx
 * @param {Function} fn(err, result)
 */
ExtendedCollection.prototype.update = function (ctx, fn) {

  var collection = this,
      store = this.store,
      session = ctx.session,
      requestedItems = [], // new data
      body = ctx.body,
      errors = {},
      isBatchQuery = false,
      domain;

  if(Array.isArray(body)) {
    requestedItems = body;
    isBatchQuery = true;
  } else if(!Array.isArray(body) && body) {
    // the user supplied a single item only
    if (typeof ctx.query.id != 'string' &&  typeof ctx.query.id != 'string') { // removed !ctx.req.isRoot, because it does not do anything
      ctx.done("queries must provide id to update an object");
      return;
    }
    body.id = body.id || ctx.query.id;
    ctx.query.id = body.id;
    requestedItems = [body];

  }
  if(!requestedItems.length) return fn('You must include an object when saving or updating.');

  // run the beforeInsert event with RAW data from the request, no previous accessible
  var eventStorage = {};
  domain = createDomain(requestedItems, errors, eventStorage);
  collection.runEvent(collection.events.BeforeUpdate, ctx, domain, function(err) {
    if(err) return fn(err);
    if(domain.hasErrors()) return fn({errors: errors});
    
    var item, 
        promises = [],
        previousItems = {}, 
        commands, 
        ids = [];
    for(var i = 0; i < requestedItems.length; i++){
      item = requestedItems[i];

      if (typeof item.id !== 'string') { // removed !ctx.req.isRoot
        ctx.done("queries must provide id to update an object");
        return;
      }
    }


    requestedItems.forEach(function(itemRequest){
      var id = itemRequest.id;
      ids.push(id);

      // build command object
      commands = {};
      Object.keys(itemRequest).forEach(function (key) {
        if(itemRequest[key] && typeof itemRequest[key] === 'object' && !Array.isArray(itemRequest[key])) {
          Object.keys(itemRequest[key]).forEach(function (k) {
            if(k[0] == '$') {
              commands[key] = itemRequest[key];
            }
          });
        }
      });

      var deferred = when.defer(),
          prev = {};

      store.find({id:id}, function(err, item){ // API change because of it does not use first
        if(err){ //TODO filter if ID was not found?
          return deferred.reject(err);
        }
        if(_.isEmpty(item)){
          var answer = deferred.reject;
          if(isBatchQuery) {
            answer = deferred.resolve;
            ctx.res.statusCode = 207; 
          }
          return answer({
            errors: {
              id: 'not found'
            },
            id: id,
            status: 400
          });
        }
        itemRequest = collection.sanitize(itemRequest);
        // copy previous obj
        Object.keys(item).forEach(function (key) {
          prev[key] = _.clone(item[key]);
        });

        // merge changes
        Object.keys(itemRequest).forEach(function (key) {
          item[key] = itemRequest[key];
        });
        previousItems[id] = prev;

        collection.execCommands('update', item, commands);
        var errs = collection.validate(item, false, ctx);

        if(errs) {
          return deferred.reject({errors: errs, id: id});
        }

        // bulk could be used here
        store.update({id: id}, item, function(err, results){ // results are the number of updated collections {count: 1}

          if(err) return deferred.reject(err);
          if(!results) {
            console.error('store update received empty result');
            return deferred.resolve();
          }

          deferred.resolve(item);
        });
      });

      promises.push(deferred.promise);
    });

    when.all(promises)
      .then(function(results){
        if(session && session.emitToAll) {
          session.emitToAll(collection.name + ':changed');
        }
        //console.log('UPDATE WHEN.ALL', results);
        // results.forEach(function(result, i){
        //   result.id = ids[i];
        // });

        // double check
        if(!Array.isArray(results)){
          results = results? [results]: [];
        }

        domain = createDomain(results, errors, eventStorage);
        domain.previous = previousItems;
        collection.runEvent(collection.events.AfterUpdate, ctx, domain, function(err) {
          if(err) return fn(err);
          if(domain.hasErrors()) return fn({errors: errors});
          
          // return single objects if the client sent a single object
          if(!isBatchQuery && !domain.hasChangedResult() && results.length <= 1){
              return fn(null, results.length? results[0] : {});
          } 
          return fn(null, domain._result);
        });
      }, function(errors){
        fn(errors);
      });
  });


    
};

ExtendedCollection.prototype.save = function (ctx, fn) {
  var collection = this,
      store = this.store,
      session = ctx.session,
      item = ctx.body,

      query = ctx.query || {},
      client = ctx.dpd,
      errors = {};

  if(!item) return done('You must include an object when saving or updating.');

  // build command object
  var commands = {};
  Object.keys(item).forEach(function (key) {
    if(item[key] && typeof item[key] === 'object' && !Array.isArray(item[key])) {
      Object.keys(item[key]).forEach(function (k) {
        if(k[0] == '$') {
          commands[key] = item[key];
        }
      });
    }
  });

  item = this.sanitize(item);

  // handle id on either body or query
  if(item.id) {
    query.id = item.id;
  }

  debug('saving %j with id %s', item, query.id);

  function done(err, item) {
    errors = domain && domain.hasErrors() && {errors: errors};
    debug('errors: %j', err);
    fn(errors || err, item);
  }

  var eventStorage = {};
  var domain = createDomain(item, errors, eventStorage);

  domain.protect = function(property) {
    delete domain.data[property];
  };

  domain.changed =  function (property) {
    if(domain.data.hasOwnProperty(property)) {
      if(domain.previous && _.isEqual(domain.previous[property], domain.data[property])) {
        return false;
      }

      return true;
    }
    return false;
  };

  domain.previous = {};

  function put() {
    var id = query.id
      , sanitizedQuery = collection.sanitizeQuery(query)
      , prev = {};

    store.first(sanitizedQuery, function(err, obj) {
      if(!obj) {
        if (Object.keys(sanitizedQuery) === 1) {
          return done(new Error("No object exists with that id"));
        } else {
          return done(new Error("No object exists that matches that query"));
        }
      }
      if(err) return done(err);

      // copy previous obj
      Object.keys(obj).forEach(function (key) {
        prev[key] = _.clone(obj[key]);
      });

      // merge changes
      Object.keys(item).forEach(function (key) {
        obj[key] = item[key];
      });

      prev.id = id;
      item = obj;
      domain['this'] = item;
      domain.data = item;
      domain.previous = prev;

      collection.execCommands('update', item, commands);

      var errs = collection.validate(item, false, ctx);

      if(errs) return done({errors: errs});

      function runPutEvent(err) {
        if(err) {
          return done(err);
        }

        if(collection.shouldRunEvent(collection.events.Put, ctx)) {
          collection.events.Put.run(ctx, domain, commit);
        } else {
          commit();
        }
      }

      function commit(err) {
        if(err || domain.hasErrors()) {
          return done(err || errors);
        }

        delete item.id;
        store.update({id: query.id}, item, function (err) {
          if(err) return done(err);
          item.id = id;

          done(null, item);

          if(session && session.emitToAll) session.emitToAll(collection.name + ':changed');
        });
      }

      if (collection.shouldRunEvent(collection.events.Validate, ctx)) {
        collection.events.Validate.run(ctx, domain, function (err) {
          if(err || domain.hasErrors()) return done(err || errors);
          runPutEvent(err);
        });
      } else {
        runPutEvent();
      }
    });
  }

  function post() {
    collection.execCommands('update', item, commands);
    var errs = collection.validate(item, true, ctx);

    if(errs) return done({errors: errs});

    // generate id before event listener
    item.id = store.createUniqueIdentifier();

    if(collection.shouldRunEvent(collection.events.Post, ctx)) {
      collection.events.Post.run(ctx, domain, function (err) {
        if(err) {
          debug('onPost script error %j', err);
          return done(err);
        }
        if(err || domain.hasErrors()) return done(err || errors);
        debug('inserting item', item);
        store.insert(item, done);
        if(session && session.emitToAll) session.emitToAll(collection.name + ':changed');
      });
    } else {
      store.insert(item, done);
      if(session && session.emitToAll) session.emitToAll(collection.name + ':changed');
    }
  }

  if (query.id) {
    put();
  } else if (collection.shouldRunEvent(collection.events.Validate, ctx)) {
    collection.events.Validate.run(ctx, domain, function (err) {
      if(err || domain.hasErrors()) return done(err || errors);
      post();
    });
  } else {
    post();
  }
};

function createDomain(data, errors, eventStorage) {
  var hasErrors = false;
  var resultChanged = false;
  var domain = {
    error: function(key, val) {
      debug('error %s %s', key, val);
      errors[key] = val || true;
      hasErrors = true;
      //throw errors; // TODO: just a temporary test
    },
    errorIf: function(condition, key, value) {
      if (condition) {
        domain.error(key, value);
      }
    },
    errorUnless: function(condition, key, value) {
      domain.errorIf(!condition, key, value);
    },
    hasErrors: function() {
      return hasErrors;
    },
    hide: function(property) {
      domain['this'].forEach(function(item) {
        delete item[property];
      });
    },
    'this': data,
    data: data,
    setResult: function(res){
      resultChanged = true;
      domain._result = res;
    },
    hasChangedResult: function() {
      return resultChanged;
    },
    result: data,
    _result: data,
    eventStorage: {
      set: function(property, value){
        if(!property || typeof property !== 'string' || property.length < 1) throw new Error('Cannot set eventStorage without a property name');
        eventStorage[property] = value;
        debug('wrote to event storage with property "%s" and value "%j"', property, value);
      },
      get: function(property){
        if(!property || typeof property !== 'string' || property.length < 1) return undefined;
        return eventStorage[property];
      },
      contains: function(property){
        if(!property || typeof property !== 'string' || property.length < 1) return false;
        return eventStorage.hasOwnProperty(property); 
      }
    }
  };
  return domain;
}

ExtendedCollection.defaultPath = '/my-objects';

ExtendedCollection.prototype.configDeleted = function(config, fn) {
  debug('resource deleted');
  return this.store.remove(fn);
};

ExtendedCollection.prototype.configChanged = function(config, fn) {
  var store = this.store;
  //console.log('NEW CONFIG', config, this.options.configPath);
  debug('resource changed');

  var properties = config && config.properties
    , renames;

  // console.log('config keys', _.keys(config));

  // should not happen parallel to index unique parsing 
  if(config.id && config.id !== this.name) {
    store.rename(config.id.replace('/', ''), function (err) {
        fn(err);
    });
    return;
  }

  if(!config.properties) return fn(null);
  var collection = this;
  var indexes = {}; // {field: filtername}

  var configPath = this.options.configPath;
  var oldConfig = this.config;
  var extendedCollectionJSON = path.join(configPath, 'extended-collection.json')
    , rConfig;

  fs.readFile(extendedCollectionJSON, {encoding: 'utf-8'}, function(err, content) {
    if(err && err.code === 'ENOENT'){
      // file does not exist
      content = '{"indexes": {}}';
    }
    else if(err){
      return fn(err);
    }
    rConfig = JSON.parse(content);
    var oldIndexes = rConfig.indexes;
    console.log('loaded old indexes', oldIndexes);

    var promises = [];
    _.keys(config.properties).forEach(function(key){

      var properties = config.properties[key];
      var oldProperties = oldConfig.properties[key] || {}; 

      // decide if something changed about indexes
      var createIndex = false
        , indexOptions = {}
        , dropIndex = false;
      
      if(!oldProperties.hasIndex && !properties.hasIndex) return;

      if(oldProperties.hasIndex && properties.hasIndex){
        if((oldProperties.isUnique && properties.isUnique) || (!oldProperties.isUnique && !properties.isUnique)) {
          return; // skip this property
        }
        else if (oldProperties.isUnique){ 
          // unique index was removed, recreate new non-unique index
          dropIndex = true;
          createIndex = true;
        } else if (properties.isUnique) { 
          // recreate unique index
          dropIndex = true;
          createIndex = true;
          indexOptions.unique = true;
        } 
      } else if (oldProperties.hasIndex) {
        // remove index
        dropIndex = true;
      } else if (properties.hasIndex) { 
        // create new non-unique index
        createIndex = true;
        if(properties.isUnique){
          indexOptions.unique = true;
        }
      }

      if(!createIndex && !dropIndex) return;

      var indexObj = {};
      indexObj[properties.id] = 1
      console.log('trying to create unique index', indexObj);
      

      // possible with only one deferred?
      var deferred = when.defer()
        , dropDeferred = when.defer()
        , db = store._db.Db;

      // drop index first (if dropIndex = true), then create if necessary.
      if(dropIndex){
        console.log('dropping index for', properties.name);
        db.collection(store.namespace).dropIndex(indexObj, function(err, result){
          if(err) return dropDeferred.reject(err);
          console.log('DROPPED INDEX', indexObj, result);
          dropDeferred.resolve();
        });
      } else {
        console.log('index does not have to get dropped for', properties.name);
        dropDeferred.resolve();
      }
      if(createIndex) {
        console.log('create index for', properties.name);
        dropDeferred.promise.then(function(){
          
          console.log('trying to create index', indexObj, indexOptions);
          db.ensureIndex(store.namespace, indexObj, indexOptions, function(err, indexName){
            if(err){
              console.error('error while creating index', indexName);
              deferred.reject(err);
              return;
            }
            indexes[properties.id] = indexName;
            console.log('created index', indexName);
            properties.index = indexName;
            deferred.resolve(indexName);
          });
        }, deferred.reject);
      } else {
        return deferred.resolve();
      }
      promises.push(deferred.promise);
    });
  
    when.all(promises).then(function(indexNames){

      if(indexNames && indexNames.length > 0){
        // there is no good way to write index names to the config.json, so use external file
        // TODO filter undefined;
        rConfig.indexes = indexes;
        var json = JSON.stringify(rConfig, null, '\t');
        fs.writeFile(extendedCollectionJSON, json, {encoding: 'utf-8'}, function(err){
          if(err) return fn(err);
          console.log('saved ' + extendedCollectionJSON);
          fn(null);
        });

      } else fn(null);
    }, function(err){
      console.error('an error occured while updating indexes:', err);
      
      // if this throws an error it is probably because there are already entries that violate the index
      if(err.name === 'MongoError'){
        if(Array.isArray(err.writeErrors)){
          var isDuplicate = err.writeErrors.reduce(function(prev, current){
            return prev || current.code === 11000;
          }, false);
          if(isDuplicate){
          console.error('writeError!', isDuplicate, err.writeErrors);
            return fn('Could not create unique index for non-unique existing data.');
          }
        }
        return fn('MongoError: '+ err.errmsg || err.message);
      } 
      fn({message: 'database error occured, check the logs', error: err});
    });
    
  });

};

ExtendedCollection.external.rename = function (options, ctx, fn) {
  if(!ctx.req && !ctx.req.isRoot) return fn(new Error('cannot rename multiple'));

  if(options.properties) {
    this.store.update({}, {$rename: options.properties}, fn);
  }
};

ExtendedCollection.prototype.execCommands = function (type, obj, commands) {
  try {
    if(type === 'update') {
      Object.keys(commands).forEach(function (key) {
        if(typeof commands[key] == 'object') {
          Object.keys(commands[key]).forEach(function (k) {
            if(k[0] !== '$') return;

            var val = commands[key][k];

            if(k === '$inc') {
              if(!obj[key]) obj[key] = 0;
              obj[key] = parseFloat(obj[key]);
              obj[key] += parseFloat(val);
            }
            if(k === '$push') {
              if(Array.isArray(obj[key])) {
                obj[key].push(val);
              } else {
                obj[key] = [val];
              }
            }
            if(k === '$pushAll') {
              if(Array.isArray(obj[key])) {
                if(Array.isArray(val)) {
                  for(var i = 0; i < val.length; i++) {
                    obj[key].push(val[i]);
                  }
                }
              } else {
                obj[key] = val;
              }
            }
            if (k === '$pull') {
              if(Array.isArray(obj[key])) {
                obj[key] = obj[key].filter(function(item) {
                  return item !== val;
                });
              }
            }
            if (k === '$pullAll') {
              if(Array.isArray(obj[key])) {
                if(Array.isArray(val)) {
                  obj[key] = obj[key].filter(function(item) {
                    return val.indexOf(item) === -1;
                  });
                }
              }
            }
            if (k === '$addUnique') {
              val = Array.isArray(val) ? val : [val];
              if(Array.isArray(obj[key])) {
                obj[key] = _.union(obj[key], val);
              } else {
                obj[key] = val;
              }
            }
          });
        }
      });
    }
  } catch(e) {
    debug('error while executing commands', type, obj, commands);
  }
  return this;
};

ExtendedCollection.prototype.shouldRunEvent = function (ev, ctx) {
  // check if a property is set on the context to ignore cascading to other events
  // used internally
  if (ctx && ctx._internalSkipEvents) return false;

  var skipEvents = ctx && ((ctx.body && ctx.body.$skipEvents) || (ctx.query && ctx.query.$skipEvents)),
      rootPrevent = ctx && ctx.session && ctx.session.isRoot && skipEvents;
  return !rootPrevent && ev;
};

ExtendedCollection.prototype.runEvent = function(ev, ctx, domain, done) {
  if(this.shouldRunEvent(ev, ctx)) {
    return ev.run(ctx, domain, done);
  }
  return done();
};

module.exports = ExtendedCollection;
