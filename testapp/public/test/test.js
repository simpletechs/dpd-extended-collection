/*global _dpd:false, $:false */
describe('extended-collection', function() {
  this.slow(200);
  var expectResultsArray = function(results){
    // debugger;
    expect(results).to.exist;
    expect(Array.isArray(results)).to.be.ok;  
  };
  describe('dpd.todos2', function() {
    it('should exist', function() {
      expect(dpd.todos2).to.exist;
    });

    describe('batch processing', function() {

      describe('insert', function() {

        it('should return an array of results for batch queries', function(done){
          dpd.todos2.post([{title: 'something'}], function(results, error){
            expectResultsArray(results);
            expect(results).to.have.lengthOf(1);
            expect(results[0]).to.have.property('title', 'something');
            done(error);
          });
        });

        it('should return an single object for non-batch queries', function(done){
          dpd.todos2.post({title: 'something'}, function(results, error){
            expect(results).to.have.property('title', 'something');
            done(error);
          });
        });

        it('should accept query array', function(done){
          dpd.todos2.post([{title: 'something'}, {title: 'somethingElse'}], function(results, error){
            expectResultsArray(results);
            expect(results).to.have.lengthOf(2);
            expect(results[0]).to.have.property('title', 'something');
            expect(results[1]).to.have.property('title', 'somethingElse');
            done(error);
          });
        });

        it('should not allow ids in the query', function(done){
          dpd.todos2.post({id: 'someId'}, function(results, error){
            expect(error).to.exist.with.property('message', 'POST does not accept objects with an id property');
            done(results);
          });
        });

        it('should be able to set result on AfterInsert', function(done){
          dpd.todos2.post({title: '$TEST_RESPONSE'}).then(function(todo){
            expect(todo).to.exist.with.property('response', true);
            done();
          }, done);
        });
      });

      describe('get', function() {

        it('should return an array of results', function(done) {
          dpd.todos2.get(function(results, err) {
            expectResultsArray(results);
            done(err);
          });
        });

        it('should allow id in the url', function(done) {
          dpd.todos2.post({title: 'something'}, function(result, err){
            if(err) return done(err);
            $.getJSON('/todos2/'+result.id).then(function(result) {
              expect(result).to.exist.with.property('title', 'something');
              done();
            }, done);
          });
        });

        it('should be able to set result on AfterGet', function(done){
          dpd.todos2.post({title: '$TEST_RESPONSE'}).then(function(todo){
            dpd.todos2.get(todo.id).then(function(todo){
              expect(todo).to.exist.with.property('response', true);
              done();
            }, done);
          }, done);
        });

        it('should allow $in on id queries', function(done){
          var id;
          chain(function(next){
            dpd.todos2.post([{title: 'something'}, {title: 'somethingElse'}], next);
          }).chain(function(next, results, error){
            expect(results).to.exist.with.lengthOf(2);
            expect(error).to.not.exist();
            id = results[0].id;
            dpd.todos2.get({id: {$in: [id]}}, next);
          }).chain(function(next, results, error){
            expect(results).to.exist.with.lengthOf(1);
            expect(results[0]).to.exist.with.property('id', id);
            done(error);
          }); 
        });

        it('should allow setQuery() in BeforeGet', function(done){
          var id;
          chain(function(next){
            dpd.todos2.post([{title: 'something'}, {title: 'somethingElse'}], next);
          }).chain(function(next, results, error){
            expect(results).to.exist.with.lengthOf(2);
            expect(error).to.not.exist();
            id = results[0].id;
            dpd.todos2.get({title: '$BEFOREGET_SETQUERY', id2: id}, next);
          }).chain(function(next, results, error){
            expect(results).to.exist.with.lengthOf(1);
            expect(results[0]).to.exist.with.property('id', id);
            done(error);
          }); 
        });
      });

      describe('update', function() {

        beforeEach(function(done){
          var test = this;
          dpd.todos2.post([{title: 'something'}, {title: 'somethingElse'}], function(results, error){
            console.log('update beforeEach', results, error);
            test.post1 = results[0];
            test.post2 = results[1];
            done(error);
          });
        });

        it('should return id of changed objects', function(done){
          var todo = this.post1;
          dpd.todos2.put(todo.id, {title: 'anotherTitle'}, function(result, error){
            expect(result).to.exist.with.property('id', todo.id);
            done(error);
          });
        });
        
        it('should accept query array', function(done){
          var todo = this.post1;
          dpd.todos2.put(todo.id, {title: 'anotherTitle'}, function(result, error){
            expect(result).to.exist.with.property('id', todo.id);
            done(error);
          });
        });

        it('should allow id in the query object', function(done) {
          var request = {
            url: '/todos2/',
            type: 'PUT',
            dataType: 'json',
            data: {
              title: 'anotherTitle1',
              id: this.post1.id
            }
          };

          $.ajax(request).then(function(result){
            expect(result).to.exist.with.property('title', 'anotherTitle1');
            done();
          }, done);
        });

        it('should allow ids in the batch query', function(done) {
          
          dpd.todos2.put([{
              title: 'anotherTitle1',
              id: this.post1.id
            }, {
              title: 'anotherTitle2',
              id: this.post2.id
            }], function(results, errors){
              if(errors) return done(errors);
              expectResultsArray(results);
              expect(results).to.have.lengthOf(2);
              expect(results[0]).to.exist.with.property('title', 'anotherTitle1');
              expect(results[1]).to.exist.with.property('title', 'anotherTitle2');
              done();
            });
        });

        it('should allow id in the url', function(done) {
          var request = {
            url: '/todos2/'+this.post1.id,
            type: 'PUT',
            dataType: 'json',
            data: {
              title: 'anotherTitle1'
            }
          };

          $.ajax(request).then(function(result){
            expect(result).to.exist.with.property('title', 'anotherTitle1');
            done();
          }, done);
        });

        it('should ignore id in the url when the request is an array', function(done) {
          var request = {
            url: '/todos2/someId',
            type: 'PUT',
            dataType: 'text',
            headers: {
              'Content-Type': 'application/json'
            },
            data: JSON.stringify([{
              title: 'anotherTitle1',
              id: this.post1.id
            }, {
              title: 'anotherTitle2',
              id: this.post2.id
            }])
          };
          
          $.ajax(request).then(function(result){
            result = JSON.parse(result);
            expect(result).to.exist().with.lengthOf(2);
            var p0 = result[0],
                p1 = result[1];

            expect(p1 !== p0).to.be.ok();
            done();
          }, done);
        });

        it('should be able to set result on AfterUpdate', function(done){
          dpd.todos2.put(this.post1.id, {title: '$TEST_RESPONSE'}).then(function(todo){
            expect(todo).to.exist.with.property('response', true);
            done();
          }, done);
        });

        it('should not crash with invalid ids', function(done){
          dpd.todos2.put('invalidId', function(result, error){
            expect(error).to.exist.with.property('errors')
              .and.with.property('id', 'not found');
            done(result);
          });
        });

        it('should return error 400 with invalid in the url', function(done){
          var request = {
            url: '/todos2/invalidId',
            type: 'PUT',
            dataType: 'json'
          };
          $.ajax(request).then(done, function(jqXHR){
            expect(jqXHR).to.exist.with.property('status', 400);

            error = JSON.parse(jqXHR.responseText);
            expect(error).to.exist.with.property('errors')
              .and.with.property('id', 'not found');
            done();
          });
        });
        

        it('should handle invalid id in batch requests with status 207', function(done) {
          var test = this;
          var request = {
            url: '/todos2/',
            type: 'PUT',
            dataType: 'text',
            headers: {
              'Content-Type': 'application/json'
            },
            data: JSON.stringify([{
              id: test.post1.id
            }, {
              id: 'invalidId'
            }])
          };
          
          $.ajax(request).then(function(results, textStatus, jqXHR){
            results = JSON.parse(results);

            expect(jqXHR).to.have.property('status', 207);
            expect(results).with.lengthOf(2);
            expect(results[0]).to.exist.not.with.property('errors')
            expect(results[0]).property('id', test.post1.id);
            
            expect(results[1]).to.exist.with.property('errors')
              .and.property('id', 'not found');
            done();
          }, done);
        });  
      });

      describe('delete', function() {

        beforeEach(function(done){
          var test = this;
          dpd.todos2.post([{title: 'something'}, {title: 'somethingElse'}], function(results, error){
            test.post1 = results[0];
            test.post2 = results[1];
            done(error);
          });
        });

        it('should delete with dpd.js', function(done){
          dpd.todos2.del(this.post1.id, function(result, error){
            if(error) return done(error);
            expect(result).to.exist.with.property('count', 1);
            done();
          });
        });

        it('should not crash with invalid ids', function(done){
          dpd.todos2.del('invalidId', function(result, error){
            expect(result).to.exist.with.property('count', 0);
            done(error);
          });
        });

        it('should not crash with invalid in the url', function(done){
          var request = {
            url: '/todos2/invalidId',
            type: 'DELETE',
            dataType: 'json'
          };
          $.ajax(request).then(function(result){
            expect(result).to.exist.with.property('count', 0);
            done();
          }, done);
        });

        it('should accept a batch query', function(done){
          var test = this;
          chain(function(next) {
            dpd.todos2.del([test.post1.id, test.post2.id], next);
          }).chain(function(next, res, err) {
            console.log('.', arguments);
            expect(res).to.have.lengthOf(2);
            dpd.todos2.get(next);
          }).chain(function(next, res) {
            expect(res).to.have.lengthOf(0);
            done();
          });
        
        });


        it('should allow id in the query object', function(done){
          var request = {
            url: '/todos2/',
            type: 'DELETE',
            dataType: 'json',
            data: {
              id: this.post1.id
            }
          };

          $.ajax(request).then(function(result){
            expect(result).to.exist.with.property('count', 1);
            done();
          }, done);
        });

        it('should allow id in the url', function(done){
          var request = {
            url: '/todos2/'+this.post1.id,
            type: 'DELETE',
            dataType: 'json'
          };

          $.ajax(request).then(function(result){
            expect(result).to.exist.with.property('count', 1);
            done();
          }, done);
        });

        it('should allow array of ids with dpd.js', function(done){
          dpd.todos2.del([this.post1.id, this.post2.id], function(result, error){
            expect(result).to.exist.with.lengthOf(2);
            result.forEach(function(r){
              expect(r).to.exist.with.property('count', 1);
            });
            done(error);
          });
        });


        it('should not expect id in the url of batch requests', function(done) {
          var request = {
            url: '/todos2',
            type: 'DELETE',
            dataType: 'text',
            headers: {
              'Content-Type': 'application/json'
            },
            data: JSON.stringify([{
              id: this.post1.id
            }, {
              id: this.post2.id
            }])
          };
          
          $.ajax(request).then(function(result){
            result = JSON.parse(result);
            expect(result).to.exist().with.lengthOf(2);
            done();
          }, done);
        });

        it('should ignore id in the url of batch requests', function(done) {
          var request = {
            url: '/todos2/someId',
            type: 'DELETE',
            dataType: 'text',
            headers: {
              'Content-Type': 'application/json'
            },
            data: JSON.stringify([{
              id: this.post1.id
            }, {
              id: this.post2.id
            }])
          };
          
          $.ajax(request).then(function(result){
            result = JSON.parse(result);
            expect(result).to.exist().with.lengthOf(2);
            var p0 = result[0],
                p1 = result[1];
            result.forEach(function(r){
              expect(r).to.exist.with.property('count', 1);
            });
            expect(p1 !== p0).to.be.ok();
            done();
          }, done);
        });

        it('should ignore ids in the query of batch requests', function(done) {
          var request = {
            url: '/todos2/?0=someId&1=invalidId',
            type: 'DELETE',
            dataType: 'text',
            headers: {
              'Content-Type': 'application/json'
            },
            data: JSON.stringify([{
              id: this.post1.id
            }, {
              id: this.post2.id
            }])
          };
          
          $.ajax(request).then(function(result){
            result = JSON.parse(result);
            expect(result).to.exist().with.lengthOf(2);
            var p0 = result[0],
                p1 = result[1];
            result.forEach(function(r){
              expect(r).to.exist.with.property('count', 1);
            });

            expect(p1 !== p0).to.be.ok();
            done();
          }, done);
        });

        it('should not crash with invalid id in batch requests', function(done) {
          var request = {
            url: '/todos2/',
            type: 'DELETE',
            dataType: 'text',
            headers: {
              'Content-Type': 'application/json'
            },
            data: JSON.stringify([{
              id: this.post1.id
            }, {
              id: 'invalidId'
            }])
          };
          
          $.ajax(request).then(function(result){
            result = JSON.parse(result);
            expect(result).to.exist.with.lengthOf(2);
            expect(result[0]).to.exist.with.property('count', 1);
            expect(result[1]).to.exist.with.property('count', 0);
            done();
          }, done);
        });
      });
    });

    describe('eventStorage', function(){

      it('should have eventStorage for GET events', function(done){
        dpd.todos2.get({eventStorage: true}, function(result, err){
          expect(result).to.exist
            .with.property('eventStorage', true);
          done(err);
        });
      });

      it('should have eventStorage for INSERT events', function(done){
        dpd.todos2.post({title: '$EVENT_STORAGE'}, function(result, err){
          expect(result).to.exist
            .with.property('eventStorage', true);
          done(err);
        });
      });
      it('should have eventStorage for UPDATE events', function(done){
        dpd.todos2.post({title: '$EVENT_STORAGE'}, function(result, err){
          dpd.todos2.put(result.id, {eventStorage: true}, function(result, err){
            expect(result).to.exist
              .with.property('eventStorage', true);
            done(err);
          });
        });
      });
      it('should have eventStorage for DELETE events', function(done){
        dpd.todos2.post({title: '$EVENT_STORAGE'}, function(result, err){
          dpd.todos2.del(result.id, function(result, err){
            expect(result).to.exist
              .with.property('eventStorage', true);
            done(err);
          });
        });
      });

    });

    describe('dpd.on("createTodo", fn)', function() {
      it('should respond to a realtime event', function(done) {
        this.timeout(1500);
        dpd.socketReady(function() {
          dpd.once('createTodo', function(todo) { //TODO :create
            expect(todo).to.exist;
            expect(todo.title).to.equal('$REALTIME');
            done();
          });

          dpd.todos2.post({title: '$REALTIME'});
        });
      });
    });

    describe('dpd.on("createTodo2", fn)', function() {
      it('should respond to a realtime event without a parameter', function(done) {
        dpd.socketReady(function() {
          dpd.once('createTodo2', function(todo) { //TODO :create
            expect(todo).to.not.exist;
            done();
          });

          dpd.todos2.post({title: '$REALTIME2'});
        });
      });
    });

    describe('dpd.todos2.on("changed", fn)', function() {
      it('should respond to the built-in changed event on post', function(done) {
        dpd.socketReady(function() {
          dpd.todos2.once('changed', function() {
            done();
          });

          dpd.todos2.post({title: 'changed - create'});
        });
      });

      it('should respond to the built-in changed event on put', function(done) {
        dpd.todos2.post({title: 'changed - create'}, function(item) {

          dpd.socketReady(function() {
            dpd.todos2.once('changed', function() {
              done();
            });

            dpd.todos2.put(item.id, {title: 'changed - updated'});
          });
        });
      });

      it('should respond to the built-in changed event on del', function(done) {
        dpd.todos2.post({title: 'changed - create'}, function(item) {
          dpd.socketReady(function() {
            dpd.todos2.once('changed', function() {
              done();
            });

            dpd.todos2.del(item.id);
          });
        });
      });
    });


    describe('.post({title: \'faux\'}, fn)', function() {
      it('should create a todo with an id', function(done) {
        dpd.todos2.post({title: 'faux'}, function (todo, err) {
          expect(todo.id.length).to.equal(16);
          expect(todo.title).to.equal('faux');
          expect(err).to.not.exist;
          done();
        });
      });
      it('should create a todo that exists in the store', function(done) {
        dpd.todos2.post({title: 'faux'}, function (todo, err) {
          expect(todo.id.length).to.equal(16);
          expect(todo.title).to.equal('faux');
          expect(err).to.not.exist;
          dpd.todos2.get(todo.id, function(res, err) {
            if (err) return done(err);
            expect(res.title).to.equal('faux');
            done();
          });
        });
      });
      it('should return a promise that is fulfilled', function(done) {
        dpd.todos2.post({title: 'faux'}).then(function(todo) {
          expect(todo.id.length).to.equal(16);
          expect(todo.title).to.equal('faux');
          done();
        });
      });
    });

    describe('.post({title: "notvalid"}, fn)', function() {
      it('should properly return an error', function(done) {
        dpd.todos2.post({title: "notvalid"}, function(result, err) {
          expect(err).to.exist;
          expect(err.errors).to.exist;
          expect(err.errors.title).to.equal("Title must not be notvalid");
          done();
        });
      });
    });

    describe('.post({}, fn)', function() {
      it('should return a validation error', function(done) {
        dpd.todos2.post({}, function(res, err) {
          expect(err).to.exist;
          expect(err.errors.title).to.be.ok;
          done();
        });
      });
    });

    describe('.post({message: "notvalid"}, fn)', function() {
      it('should properly return an error', function(done) {
        dpd.todos2.post({message: "notvalid"}, function(result, err) {
          console.log('message notvalid', result, err);
          expect(err).to.exist;
          expect(err.errors).to.exist;
          expect(err.errors.message).to.equal("Message must not be notvalid");
          done();
        });
      });

      it('should return a promise that is rejected', function(done) {
        dpd.todos2.post({message: "notvalid"}).then(function(todos) {
          done('promise should not be fulfilled');
        }, function(err) {
          expect(err).to.exist;
          expect(err.errors).to.exist;
          expect(err.errors.message).to.equal("Message must not be notvalid");
          done();
        });
      });

      it('should not post the message', function(done) {
        chain(function(next) {
          dpd.todos2.post({title: "$POSTERROR"}, next);
        }).chain(function(next, res, err) {
          expect(err).to.exist;
          expect(err.errors).to.exist;
          expect(err.errors.title).to.equal("POST error");
          dpd.todos2.get(next);
        }).chain(function(next, res) {
          expect(res.length).to.equal(0);
          done();
        });
      });

    });

    describe('.post({title: "foo", owner: 7}, fn)', function() {
      it('should coerce numbers to strings when querying string properties', function(done) {
        dpd.todos2.post({title: "foo", owner: 7}, function (todo, err) {          
          console.error('COERCE', todo, err); //TODO returns err owner must be a string, which might be better.
          //expectResultsArray(todo);
          delete todo.id;
          expect(todo).to.eql({title: "foo", done: false, owner: '7'});
          done();
        });
      });
    });

    describe('.post({title: "$TESTFAIL", fn)', function() {
      it('should correctly respond to errors in event IO', function(done) {
        dpd.todos2.post({title: "$TESTFAIL"}, function(todo, err) {
          expect(todo.err).to.exist;
          done();
        });
      });
    });

    describe('.get(fn)', function() {
      it('should not return anything when get was canceled', function(done) {
        chain(function(next) {
          dpd.todos2.post({title: "This one is OK"}, next);
        }).chain(function(next, res, err) {
          if (err) return done(err);
          dpd.todos2.post({title: "$GET_CANCEL"}, next);
        }).chain(function(next, res, err) {
          if (err) return done(err);
          dpd.todos2.get(next);
        }).chain(function(next, res, err) {
          console.log('get canceled',res, err);
          expect(err).to.exist.with.property('message', 'Canceled');
          done(res);
        });
      });
    });

    describe('.get({title: title}, fn)', function() {
      it('should return a single result', function(done) {
        var title = Math.random().toString();

        dpd.todos2.post({title: title}, function () {
          dpd.todos2.post({title: "Some other"}, function() {
            dpd.todos2.get({title: "Some other"}, function (todos, err) {
              expect(todos.length).to.equal(1);
              done(err);
            });
          });
        });
      });
    });

    describe('.get({$sort: {title: 1}}, fn)', function() {
      it('should order by title', function(done) {
        chain(function(next) {
          dpd.todos2.post({title: "C"}, next);
        }).chain(function(next) {
          dpd.todos2.post({title: "A"}, next);
        }).chain(function(next) {
          dpd.todos2.post({title: "B"}, next);
        }).chain(function(next) {
          dpd.todos2.get({$sort: {title: 1}}, next);
        }).chain(function(next, result, err) {
          expect(result).to.exist;
          expect(result.length).to.equal(3);
          expect(result[0].title).to.equal("A");
          expect(result[1].title).to.equal("B");
          expect(result[2].title).to.equal("C");
          done(err);
        });
      });
    });

    describe('.get({id: {$ne: "..."}}, fn)', function() {
      it('should return all results that do not match the given id', function(done) {
        var titleA = Math.random().toString()
          , titleB = Math.random().toString();

        dpd.todos2.post({title: titleA}, function () {
          dpd.todos2.post({title: titleB}, function () {
            dpd.todos2.get({title: {$ne: titleA}}, function (todos, err) {
              expect(todos.length).to.equal(1);
              expect(todos[0].title).to.not.equal(titleA);
              var id = todos[0].id;
              dpd.todos2.get({id: {$ne: id}}, function (todos, err) {
                expect(todos.length).to.equal(1);
                expect(todos[0].id).to.not.equal(id);
                done(err);
              });
            });
          });
        });
      });
    });

    describe('.get({id: "non existent"}, fn)', function() {
      it('should return a 404', function(done) {
        dpd.todos2.get({id: "non existent"}, function (todo, err) {
          expect(todo).to.not.exist;
          expect(err.message).to.equal('not found');
          expect(err.statusCode).to.equal(404);
          done();
        });
      });
    });

    describe('.get({title: "$TESTFAIL2"}, fn)', function() {
      it('should correctly respond to errors in event IO', function(done) {
        dpd.todos2.post({title: "$FAIL2"}, function() {
          dpd.todos2.post({title: "$TESTFAIL2"}, function() {
            dpd.todos2.get({title: "$TESTFAIL2"}, function(todos, err) {
              expect(todos).to.exist()
              expect(todos[0]).with.property('err');
              done();
            });
          });
        });
      });
    });

    describe('.get(27, fn)', function() { //TODO validate type? sanitization might not work correctly
      it('should not hang if given a number as id', function(done) {
        dpd.todos2.post({title: 'foobar'}, function () {
          dpd.todos2.get(27, function (todos, err) {
            expect(err).to.exist;
            done();
          });
        });
      });
    });

    describe('.get({numberGet: true}, fn)', function() {
      it('should not hang if given a number as id', function(done) {
        dpd.todos2.post({title: 'foobar'}, function () {
          dpd.todos2.get({numberGet: true}, function (todos, err) {
            expect(todos.length).to.equal(1);
            expect(todos[0].numberGet).to.equal('noResponse');
            done(err);
          });
        });
      });
    });

    describe('.get({arbitrary: true}, fn)', function() { 
      it('should allow arbitrary query parameters', function(done) {
        dpd.todos2.post({title: 'foobar'}, function () {
          dpd.todos2.get({arbitrary: true}, function (todos, err) {
            console.log('arbitrary', todos);
            expect(todos.length).to.equal(1);
            expect(todos[0].custom).to.equal('arbitrary');
            done(err);
          });
        });
      });

      it('should run events when an id is included', function(done) {
        dpd.todos2.post({title: 'foobar'}, function (todo) {
          dpd.todos2.get({arbitrary: true, id: todo.id}, function (todo) {
            expect(todo.custom).to.equal('arbitrary');
            done();
          });
        });
      });
    });

    describe('.get(id, fn)', function() {
      it('should run events when an id is queried', function(done) {
        dpd.todos2.post({title: 'foobar'}, function (todo) {
          dpd.todos2.get(todo.id, function (t) {
            expect(t.custom).to.equal('custom');
            done();
          });
        });
      });
    });

    describe('.get({"people.info.name": "Tom"}, fn)', function() {
      it('should create a todo', function(done) {
        var tom = {name: 'Tom', age: 13};
        var june = {name: 'June', age: 27};
        var post1 = {title: 'Some post', people: {info: tom}};
        var post2 = {title: 'Another post', people: {info: june}};

        dpd.todos2.post(post1, function (todos, result) {
          var todo = todos;
          expect(todo.people.info.name).to.equal('Tom');
          dpd.todos2.post(post2, function (todos) {
            var todo = todos;
            dpd.todos2.get(function (todos) {
              expect(todos.length).to.equal(2);
            });
            dpd.todos2.get({'people.info.name': 'Tom'}, function (todos) {
              expect(todos.length).to.equal(1);
              expect(todos[0].people.info.name).to.equal('Tom');
              done();
            });
          });
        });
      });
    });

    describe('.put(id, {title: "todo 2"}, {done: true},  fn)', function() {
      xit('should throw an error if the filter does not apply', function(done) { //TODO filters not implemented?
        var todoId;
        chain(function(next) {
          dpd.todos2.post({title: 'todo 1'}, next);
        }).chain(function(next, results, err) {
          if (err) return done(err);
          var res = results;
          todoId = res.id;
          dpd.todos2.put(todoId, {title: "todo 2"}, {done: true}, next);
        }).chain(function(next, results, err) {
          console.log(arguments);
          expect(err).to.exist;
          dpd.todos2.get(todoId, next);
        }).chain(function(next, results, err) {
          expect(results.done).to.be('false');
          done();
        });
      });
    });

    describe('.put(id, {done: true}, fn)', function() {
      it('should be able to access old properties in On AfterUpdate', function(done) {
        chain(function(next) {
          dpd.todos2.post({title: '$PUT_TEST', message: "x"}, next);
        }).chain(function(next, result) {
          dpd.todos2.put(result.id, {done: true}, next);
        }).chain(function(next, result) {
          expect(result.message).to.equal("xx");
          dpd.todos2.get(result.id, next);
        }).chain(function(next, result) {
          //expect(result.message).to.equal("xx"); // no longer valid due to being OnAfterUpdate
          expect(result.done).to.equal(true);
          done();
        });
      });

      it('should be able to manipulate query in On BeforeUpdate', function(done) { // no validate anymore
        chain(function(next) {
          dpd.todos2.post({title: '$BEFOREUPDATE_TEST', message: ""}, next);
        }).chain(function(next, result) {
          dpd.todos2.put(result.id, {done: true, title: '$BEFOREUPDATE_TEST'}, next);
        }).chain(function(next, result) {
          console.log('.', arguments);
          expect(result.message).to.equal("x");
          dpd.todos2.get(result.id, next);
        }).chain(function(next, result) {
          console.log('..', arguments);
          expect(result.message).to.equal("x");
          expect(result.done).to.equal(true);
          done();
        });
      });

      it('should add properties', function(done) {
        var id;
        chain(function(next) {
          dpd.todos2.post({title: 'foobar'}, next);
        }).chain(function(next, result) {
          dpd.todos2.put(result.id, {done: true}, next);
        }).chain(function(next, result) {
          expect(result.title).to.equal('foobar');
          expect(result.done).to.equal(true);
          done();
        });
      });
    });

    describe('.put(id, {message: "notvalid"}, fn)', function() {
      it('should cancel the update', function(done) {
        var todoId;
        chain(function(next) {
          dpd.todos2.post({title: "Some todo"}, next);
        }).chain(function(next, res, err) {
          if (err) return done(err);
          todoId = res.id;
          dpd.todos2.put(todoId, {message: "notvalid"}, next);
        }).chain(function(next, res, err) {
          expect(err).to.exist;
          expect(err.errors).to.exist;
          expect(err.errors.message).to.equal("Message must not be notvalid");
          dpd.todos2.get(todoId, next);
        }).chain(function(next, res, err) {
          if (err) return done(err);
          expect(res.message).to.not.equal("notvalid");
          done();
        });
      });
    });

    describe('.put(id, {message: "notvalidput"}, fn)', function() {
      it('should cancel the update', function(done) {
        var todoId;
        chain(function(next) {
          dpd.todos2.post({title: "Some todo"}, next);
        }).chain(function(next, res, err) {
          if (err) return done(err);
          todoId = res.id;
          dpd.todos2.put(todoId, {message: "notvalidput"}, next);
        }).chain(function(next, res, err) {
          expect(err).to.exist;
          expect(err.errors).to.exist;
          expect(err.errors.message).to.equal("message should not be notvalidput");
          dpd.todos2.get(todoId, next);
        }).chain(function(next, res, err) {
          if (err) return done(err);
          expect(res.message).to.not.equal("notvalidput");
          done();
        });
      });
    });


    describe('.put(id, {tags: ["red", "blue"]}, fn)', function() {
      it('should set an array', function(done) {
        chain(function(next) {
          dpd.todos2.post({title: 'foobar'}, next);
        }).chain(function(next, result) {
          dpd.todos2.put(result.id, {tags: ["red", "blue"]}, next);
        }).chain(function(next, result) {
          dpd.todos2.get(result.id, next);
        }).chain(function(next, results) {
          var result = results;
          expect(result.tags).to.exist();
          expect(result.tags.length).to.equal(2);
          expect(result.tags).to.include("red").and.include("blue");
          dpd.todos2.get(result.id, next);
        }).chain(function(next, result) {
          expect(result.tags).to.exist();
          expect(result.tags.length).to.equal(2);
          expect(result.tags).to.include("red").and.include("blue");
          done();
        });
      });
    });

    describe('.put(id, {tags: {$push: "red"}}, fn)', function() { //TODO SSS-213
      it('should update an array', function(done) {
        chain(function(next) {
          dpd.todos2.post({title: 'foobar', tags: ['blue']}, next);
        }).chain(function(next, result) {
          dpd.todos2.put(result.id, {tags: {$push: "red"}}, next);
        }).chain(function(next, result) {
          console.log(arguments);
          dpd.todos2.get(result.id, next);
        }).chain(function(next, result) {
          expect(result.tags.length).to.equal(2);
          expect(result.tags).to.include("red").and.include("blue");
          done();
        });
      });

      it('should update an empty array', function(done) { //TODO SSS-213
        chain(function(next) {
          dpd.todos2.post({title: 'foobar'}, next);
        }).chain(function(next, result) {
          dpd.todos2.put(result.id, {tags: {$push: "red"}}, next);
        }).chain(function(next, result) {
          dpd.todos2.get(result.id, next);
        }).chain(function(next, result) {
          console.log('tags', arguments);
          expect(result.tags.length).to.equal(1);
          expect(result.tags).to.include("red");
          done();
        });
      });
    });

    describe('.put(id, {tags: {$pushAll: ["red", "yellow"]}}, fn)', function() { //TODO SSS-213
      it('should update an array', function(done) {
        chain(function(next) {
          dpd.todos2.post({title: 'foobar', tags: ['blue']}, next);
        }).chain(function(next, result) {
          dpd.todos2.put(result.id, {tags: {$pushAll: ["red", "yellow"]}}, next);
        }).chain(function(next, result) {
          expect(result.tags.length).to.equal(3);
          expect(result.tags).to.include("red").and.include("blue").and.include("yellow");
          done();
        });
      });
    });

    describe('.put(id, tags: {$pull: "red"}, fn)', function() { //TODO SSS-213
      it('should remove an item from an array', function(done) {
        chain(function(next) {
          dpd.todos2.post({title: 'foobar', tags: ['red', 'blue']}, next);
        }).chain(function(next, result) {
          dpd.todos2.put(result.id, {tags: {$pull: "red"}}, next);
        }).chain(function(next, result) {
          expect(result.tags.length).to.equal(1);
          expect(result.tags).to.include("blue");
          done();
        });
      });
    });

    describe('.put(id, tags: {$pullAll: ["red", "blue"]}, fn)', function() { //TODO SSS-213
      it('should remove multiple items from an array', function(done) {
        chain(function(next) {
          dpd.todos2.post({title: 'foobar', tags: ['red', 'blue', 'yellow']}, next);
        }).chain(function(next, result) {
          dpd.todos2.put(result.id, {tags: {$pullAll: ["red", "blue"]}}, next);
        }).chain(function(next, result) {
          expect(result.tags.length).to.equal(1);
          expect(result.tags).to.include("yellow");
          done();
        });
      });
    });

    describe('.put(id, {tags: {$addUnique: "red"}}, fn)', function() { //TODO SSS-213
      it('should update a set', function(done) {
        chain(function(next) {
          dpd.todos2.post({title: 'foobar', tags: ['blue', 'green']}, next);
        }).chain(function(next, result) {
          dpd.todos2.put(result.id, {tags: {$addUnique: "red"}}, next);
        }).chain(function(next, result) {
          expect(result.tags.length).to.equal(3);
          expect(result.tags).to.include("red").and.include("blue").and.include("green");
          done();
        });
      });

      it('should update an empty array', function(done) {
        chain(function(next) {
          dpd.todos2.post({title: 'foobar'}, next);
        }).chain(function(next, result) {
          dpd.todos2.put(result.id, {tags: {$addUnique: "red"}}, next);
        }).chain(function(next, result) {
          expect(result.tags.length).to.equal(1);
          expect(result.tags).to.include("red");
          done();
        });
      });

      it('should not update a set if element already exists', function(done) {
        chain(function(next) {
          dpd.todos2.post({title: 'foobar', tags: ['red', 'green']}, next);
        }).chain(function(next, result) {
          dpd.todos2.put(result.id, {tags: {$addUnique: "red"}}, next);
        }).chain(function(next, result) {
          expect(result.tags.length).to.equal(2);
          expect(result.tags).to.include("red").and.include("green");
          done();
        });
      });

    });

    describe('.put(id, {tags: {$addUnique: ["yellow", "magenta", "violet"]}}, fn)', function() {  //TODO SSS-213
      it('should update a set', function(done) {
        chain(function(next) {
          dpd.todos2.post({title: 'foobar', tags: ['red', 'blue', 'green']}, next);
        }).chain(function(next, result) {
          dpd.todos2.put(result.id, {tags: {$addUnique: ["yellow", "magenta", "violet"]}}, next);
        }).chain(function(next, result) {
          expect(result.tags.length).to.equal(6);
          expect(result.tags).to.include("red").and.include("blue").and.include("green")
            .and.include("yellow").and.include("magenta").and.include("violet");
          done();
        });
      });

      it('should update an empty array', function(done) {
        chain(function(next) {
          dpd.todos2.post({title: 'foobar'}, next);
        }).chain(function(next, result) {
          dpd.todos2.put(result.id, {tags: {$addUnique: ["yellow", "magenta", "violet"]}}, next);
        }).chain(function(next, result) {
          expect(result.tags.length).to.equal(3);
          expect(result.tags).to.include("yellow").and.include("magenta").and.include("violet");
          done();
        });
      });

      it('should not update a set if element already exists', function(done) {
        chain(function(next) {
          dpd.todos2.post({title: 'foobar', tags: ['red', 'blue', 'green', 'yellow', 'magenta']}, next);
        }).chain(function(next, result) {
          dpd.todos2.put(result.id, {tags: {$addUnique: ["yellow", "magenta", "violet"]}}, next);
        }).chain(function(next, result) {
          expect(result.tags.length).to.equal(6);
          expect(result.tags).to.include("red").and.include("blue").and.include("green")
            .and.include("yellow").and.include("magenta").and.include("violet");
          done();
        });
      });

    });

    describe('.put({done: true})', function(){
      it('should not update multiple items', function(done) {
        chain(function(next) {
          dpd.todos2.post({title: 'foo'}, next);
        }).chain(function(next) {
          dpd.todos2.post({title: 'bar'}, next);
        }).chain(function(next, result) {
          dpd.todos2.put({done: true}, function (res, err) {
            expect(err).to.exist;
            done();
          });
        });
      });
    });

    describe('dpd.todos2 method override', function() {
      it('should method overriden by _method property from body', function(done){
        var todoId;
        chain(function(next) {
          dpd.todos2.post({title: "Some todo"}, next);
        }).chain(function(next, res, err) {
          if (err) return done(err);
            todoId = res.id;
          expect(res).to.exist;
          $.ajax({
            type: "POST",
            url : "/todos2/"+ todoId,
            data: { _method:"DELETE" },
            success: function(res) {
              dpd.todos2.get(todoId, function(result){
                  expect(result).to.not.exist;
                  done();
              });
            },
            error: function (e) {
                if (e) return done(e);
              done();
            }
          });
        });
      });
      it('should method overriden by _method property from url', function(done){
        var todoId;
        chain(function(next) {
          dpd.todos2.post({title: "Some todo"}, next);
        }).chain(function(next, res, err) {
          if (err) return done(err);
            todoId = res.id;
          expect(res).to.exist;
          $.ajax({
            type: "POST",
            url : "/todos2/"+ todoId + "?_method=delete",
            success: function(res) {
              dpd.todos2.get(todoId, function(result){
                  expect(result).to.not.exist;
                  done();
              });
            },
            error: function (e) {
                if (e) return done(e);
              done();
            }
          });
        });
      });

      it('should not deleted by executed commands it like dpd.collection.cmd ', function(done){
        var todoId;
        chain(function(next) {
          dpd.todos2.post({title: "Some todo"}, next);
        }).chain(function(next, res, err) {
          if (err) { return done(err); }
          todoId = res.id;
          expect(res).to.exist;
          dpd.todos2.post({ _method:"delete"}, next);
        }).chain(function(next, res, err){
          dpd.todos2.get({ id:todoId }, next);
        }).chain(function(next, res, err){
          expect(res).to.exist;
          done();
        });
      });
    });

    afterEach(function (done) {
      this.timeout(10000);
      cleanCollection(dpd.todos2, done);
    });

  });

  describe('issue 76', function() {
    it('should prevent unauthorized post', function(done) {
      chain(function(next) {
        dpd.todos2.post({title: "$REQUIRE_AUTH"}, next);
      }).chain(function(next, res, err) {
        console.log('issue 76', arguments);
        expect(err).to.exist;
        done();
      });
    });

    it('should allow logged in user to post', function(done) {
      chain(function(next) {
        dpd.users.post({username: 'foo', password: 'bar'}, next);
      }).chain(function(next) {
        dpd.users.login({username: 'foo', password: 'bar'}, next);
      }).chain(function(next) {
        dpd.todos2.post({title: "$REQUIRE_AUTH"}, next);
      }).chain(function(next, res, err) {
        expect(err).to.not.exist;
        expect(res.title).to.equal("$REQUIRE_AUTH");
        done();
      });
    });

    it('should allow logged in user to post after second try', function(done) {
      chain(function(next) {
        dpd.users.post({username: 'foo', password: 'bar'}, next);
      }).chain(function(next) {
        dpd.users.login({username: 'foo', password: 'bar'}, next);
      }).chain(function(next) {
        dpd.todos2.post({title: "$REQUIRE_AUTH"}, next);
      }).chain(function(next) {
        dpd.todos2.post({title: "$REQUIRE_AUTH"}, next);
      }).chain(function(next, res, err) {
        expect(err).to.not.exist;
        expect(res.title).to.equal("$REQUIRE_AUTH");
        done();
      });
    });


    afterEach(function(done) {
      this.timeout(10000);
      dpd.users.logout(function() {
        cleanCollection(dpd.users, function() {
          cleanCollection(dpd.todos2, done);
        });
      });
    });
  });

  describe('internal cancel()', function(){
    it('should not cancel the internal call', function(done) {
      dpd.todos2.post({title: '$CANCEL_TEST'}, function (todo) {
        expect(todo.err).to.not.exist;
        dpd.todos2.get({title: '$INTERNAL_CANCEL_TEST'}, function (todos) {
          expect(todos.length).to.equal(1);
          done();
        });
      });
    });



    afterEach(function (done) {
      this.timeout(10000);
      cleanCollection(dpd.todos2, done);
    });
  });

  describe('events', function() {
    describe('cancelIf()', function() {
      it('should cancel', function(done) {
        dpd.todos2.post({title: "$CANCEL_IF_TEST"}, function(todo, err) {
          expect(err).to.exist;
          expect(err.message).to.equal("Cancel if");
          done();
        });
      });
    });

    describe('cancelUnless()', function() {
      it('should cancel', function(done) {
        dpd.todos2.post({title: "$CANCEL_UNLESS_TEST"}, function(todo, err) {
          expect(err).to.exist;
          expect(err.message).to.equal("Cancel unless");
          done();
        });
      });
    });

    describe('hasErrors()', function() {
      it('should cancel', function(done) {
        dpd.todos2.post({title: "$HAS_ERRORS_TEST"}, function(todo, err) {
          expect(err).to.exist;
          expect(err.errors.hasErrors).to.equal("Yep");
          expect(err.errors.otherError).to.exist;
          done();
        });
      });
    });

    describe('errorIf()', function() {
      it('should error', function(done) {
        dpd.todos2.post({title: "$ERROR_IF_TEST"}, function(todo, err) {
          expect(err).to.exist;
          expect(err.errors).to.exist;
          expect(err.errors.errorIf).to.equal("Yep");
          done();
        });
      });
    });

    describe('errorUnless()', function() {
      it('should error', function(done) {
        dpd.todos2.post({title: "$ERROR_UNLESS_TEST"}, function(todo, err) {
          expect(err).to.exist;
          expect(err.errors).to.exist;
          expect(err.errors.errorUnless).to.equal("Yep");
          done();
        });
      });
    });
  });

  describe('root', function() {
    afterEach(function(done) {
      _dpd.ajax.headers = {};
      cleanCollection(dpd.todos2, done);
    });

    describe('dpd-ssh-key', function() {
      beforeEach(function() {
        _dpd.ajax.headers = {
          'dpd-ssh-key': true
        };
      });

      it('should detect root', function(done) {
        chain(function(next) {
          dpd.todos2.post({title: 'valid'}, next);
        }).chain(function(next, res, err) {
          if (err) return done(err);
          expect(res.isRoot).to.equal(true);
          done();
        });
      });

      it('should allow skipping events', function(done) {
        chain(function(next) {
          dpd.todos2.post({title: 'notvalid', $skipEvents: true}, next);
        }).chain(function(next, res, err) {
          if (err) return done(err);
          expect(res.title).to.equal('notvalid');
          done();
        });
      });

      it('should allow skipping events on get', function(done) {
        var id;
        chain(function(next) {
          dpd.todos2.post({title: '$GET_CANCEL'}, next);
        }).chain(function(next, res, err) {
          if (err) return done(err);
          id = res.id;
          dpd.todos2.get(id, {$skipEvents: true}, next);
        }).chain(function(next, res, err) {
          if (err) return done(err);
          expect(res.title).to.equal("$GET_CANCEL");
          done();
        });
      });
    });

    it('should not allow skipping events', function(done) {
      chain(function(next) {
        dpd.todos2.post({title: 'notvalid', $skipEvents: true}, next);
      }).chain(function(next, res, err) {
        expect(err).to.exist;
        expect(err.errors).to.exist;
        done();
      });
    });

    it('should not detect root', function(done) {
      chain(function(next) {
        dpd.todos2.post({title: 'valid'}, next);
      }).chain(function(next, res, err) {
        if (err) return done(err);
        expect(res.isRoot).to.not.exist;
        done();
      });
    });

  });

  describe('internal client', function () {
    before(function(done) {
      cleanCollection(dpd.internalclientmaster, done);
    });
    
    function populate(children) {
      var masterId;
      return dpd.internalclientmaster.post({ title: "hello" }).then(function (data) {
        masterId = data.id;
        return dpd.internalclientdetail.post({ masterId: masterId, data: "data 1" });
      }).then(function (data) {
        children.push(data);
        return dpd.internalclientdetail.post({ masterId: masterId, data: "data 2" });
      }).then(function (data) {
        children.push(data);
        return masterId;
      });
    }
    
    it("should work properly with callbacks", function (done) {
      var children = [];
      populate(children).then(function (masterId) {
        return dpd.internalclientmaster.get({ id: masterId, callback: true });
      }).then(function (master) {
        expect(master.children).to.eql(children);
        done();
      }).fail(function (err) {
        done(err);
      });
    });
      
    it("should work properly with promises", function (done) {
      var children = [];
      populate(children).then(function (masterId) {
        return dpd.internalclientmaster.get({ id: masterId, promise: true });
      }).then(function (master) {
        expect(master.childrenPromise).to.eql(children);
        expect(master.seenFinally).to.be.true;
        expect(master.seenError).to.equal('test');
        expect(master.shouldNotBeSet).to.not.exist;
        expect(master.shouldNotBeSet2).to.not.exist;
        done();
      }).fail(function (err) {
        done(err);
      });
    });

    it("should work properly with both normal callbacks and promises at the same time", function (done) {
      var children = [];
      populate(children).then(function (masterId) {
        return dpd.internalclientmaster.get({ id: masterId, callback: true, promise: true });
      }).then(function (master) {
        expect(master.children).to.eql(children);
        expect(master.childrenPromise).to.eql(children);
        expect(master.seenFinally).to.be.true;
        expect(master.seenError).to.equal('test');
        expect(master.shouldNotBeSet).to.not.exist;
        expect(master.shouldNotBeSet2).to.not.exist;
        done();
      }).fail(function (err) {
        done(err);
      });
    });
      
    it("should properly report uncaught error in callback and promise", function (done) {
      var masterId;
      var children = [];
      populate(children).then(function (mid){
        masterId = mid;
        return dpd.internalclientmaster.get({ id: masterId, promise: true, testUncaughtError: true });
      }).then(function () {
        throw "an error should've been returned";
      }, function (err) {
        expect(err).to.exist;
        expect(err.message).to.equal('fail');
        return dpd.internalclientmaster.get({ id: masterId, callback: true, testUncaughtError: true });
      }).then(function() {
        throw "an error should've been returned"; 
      }, function (err) {
        expect(err).to.exist;
        expect(err.message).to.equal('fail');
        done();
      });
    });
  });
  


});
