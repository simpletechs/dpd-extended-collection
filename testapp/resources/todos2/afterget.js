
if(query.eventStorage){
    console.log('EVENT STORAGE TEST!', eventStorage);
    setResult({
        eventStorage: eventStorage.get('test')
    });
    return;
}
data.forEach(function(item) {
    item.custom = 'custom';

    if(!query.clean && item.title === '$TEST_RESPONSE'){
        return setResult({response: true});
    }
    
    if (query.numberGet) {
        dpd.todos.get(27, function(res) {
            item.numberGet = res ? "response" : "noResponse";
        });
    }

    if (query.arbitrary) {
        item.custom = 'arbitrary';
    }
    
    if (query.title === "$TESTFAIL2") {
        dpd.todos.get({title: "$FAIL2"}, function(todo, err) {
            item.todo = todo;
            item.err = err;
        });
    }


    if (item.title === "$GET_CANCEL" && !query.clean) {
      cancel("Canceled");
    }
});
