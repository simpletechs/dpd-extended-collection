
data.forEach(function(item) {

    if(item.title === '$EVENT_STORAGE'){
        item.eventStorage = eventStorage.get('test');
    }
    
    if(item.title === '$TEST_RESPONSE'){
        return setResult({response: true});
    }
    
    // former put.js        
    if (item.title === "$PUT_TEST") {
      item.message += "x";
    }
});