data.forEach(function(item) {
    if (item.message === 'notvalid') {
        error('message', "Message must not be notvalid");
    }
    
    if (item.title === 'notvalid') {
        error('title', "Title must not be notvalid");
    }
    
    if (item.title === '$VALIDATE_TEST') {
      item.message += "x";
    }
    
    errorIf(item.title === "$ERROR_IF_TEST", 'errorIf', "Yep");
    errorUnless(item.title !== "$ERROR_UNLESS_TEST", 'errorUnless', "Yep");

});

eventStorage.set('test', true);