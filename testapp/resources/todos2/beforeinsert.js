data.forEach(function(item) {
    if (item.message === 'notvalid') {
        error('message', "Message must not be notvalid");
    }
    
    if (item.title === 'notvalid') {
        error('title', "Title must not be notvalid");
    }
    
    errorIf(item.title === "$ERROR_IF_TEST", 'errorIf', "Yep");
    errorUnless(item.title !== "$ERROR_UNLESS_TEST", 'errorUnless', "Yep");
    
    // former post.js
    if (item.title == "$REQUIRE_AUTH") {
        if (!me) cancel("You are not authorized", 401);
    }
    
    if (item.title === "$POSTERROR") {
        error('title', "POST error");
    }
    
    if (item.title === "$FAIL") {
        var x = null;
        x.fail();
    }
    
    if (item.title === "$REALTIME") {
        emit('createTodo', item);    
    }
    
    if (item.title === "$REALTIME2") {
        emit('createTodo2');    
    }
    
    
    if (item.title === "$INTERNAL_CANCEL_TEST") {
      if (!internal) cancel('internal cancel');
    }
    
    
    cancelIf(item.title === "$CANCEL_IF_TEST", "Cancel if");
    cancelUnless(item.title !== "$CANCEL_UNLESS_TEST", "Cancel unless");
    
    if (item.title === "$HAS_ERRORS_TEST") {
      error('hasErrors', "Yep");
      if (hasErrors()) {
          error('otherError', "Yep");
      }
    }
});

eventStorage.set('test', true);