data.forEach(function(item) {
    if(item.title === '$EVENT_STORAGE'){
        item.eventStorage = eventStorage.get('test');
    }

    if(item.title === '$TEST_RESPONSE'){
        return setResult({response: true, id: item.id});
    }
    // former validate
    if (item.title === '$VALIDATE_TEST') {
      item.message += "x";
    }
    
    // former post.js
    if (item.title === "$TESTFAIL") {
        dpd.todos2.post({title: "$FAIL"}, function(results, err) {
            item.results = results;
            item.err = err;
        });
    }
    
    if (item.title === "$CANCEL_TEST") {
      dpd.todos2.post({title: "$INTERNAL_CANCEL_TEST"}, function (results, err) {
        item.err = err;
        item.results = results;
      });
    }
    if (isRoot) {
        item.isRoot = true;
    }
});