data.forEach(function(item) {
    // from validate
    if (item.title === '$BEFOREUPDATE_TEST') {
      item.message = "x";
    }

    // former onPut
    if (item.message === 'notvalid') {
        error('message', "Message must not be notvalid");
    }
    
    if (item.title === 'notvalid') {
        error('title', "Title must not be notvalid");
    }

    if (item.message == "notvalidput") {
        error('message', "message should not be notvalidput");
    }

    errorIf(item.title === "$ERROR_IF_TEST", 'errorIf', "Yep");
    errorUnless(item.title !== "$ERROR_UNLESS_TEST", 'errorUnless', "Yep");

});

eventStorage.set('test', true);